<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\OTP;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $otp;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->generateOtp($user);
    }

    private function generateOtp($user){
        $otp = new OTP();
        $otp->user_id = $user->id;
        $otp->otp = random_int(10000,99999);
        $otp->action = 'forgot_password';
        $otp->available_until = strtotime("+15 min");
        $otp->save();
        $this->otp = $otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('seno@divistant.com', 'Iya Laundry')
            ->subject('Iya Laundry, Percobaan perubahaan password')
            ->withSwiftMessage(function ($message) {
                $message->getHeaders()->addTextHeader('X-PM-Message-Stream', 'outbound');
            })
            ->view('emails.forgot_password', ['user'=>$this->user, 'otp'=>$this->otp]);
    }
}
