<?php
namespace App\Exceptions;

use Exception;

class CoreException extends Exception{
    protected $status;
    protected $details;

    function __construct(String $message, int $status, mixed $details = null){
        $this->message = $message;
        $this->status = $status;
        $this->details = $details;
    }

    function getStatus(){
        return $this->status;
    }

    function getDetails(){
        return $this->details;
    }
}