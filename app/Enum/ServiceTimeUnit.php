<?php
namespace App\Enum;

enum ServiceTimeUnit: String {
    case DAY = "DAY";
    case HOUR = "HOUR";
}