<?php
namespace App\Enum;

enum ServiceUnit: String {
    case KG = "KG";
    case G = "G";
    case PCS = "PCS";
}