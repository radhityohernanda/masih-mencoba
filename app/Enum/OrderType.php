<?php

namespace App\Enum;

enum OrderType: string{
    case OFFLINE = "OFFLINE";
    case ONLINE = "ONLINE";
    case SUBSCRIPTION = "SUBSCRIPTION";
}
