<?php
namespace App\Enum;

enum PaymentStatus: String {
    case CART = "0";
    case WAITING = "1";
    case DONE = "2";
    case EXPIRED = "3";
    case CANCEL = "4";
}