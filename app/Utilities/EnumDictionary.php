<?php
namespace App\Utilities;

use App\Enum\ServiceTimeUnit;
use App\Enum\ServiceUnit;

use function PHPSTORM_META\map;

class EnumDictionary{

    public static function ServiceTimeUnit(ServiceTimeUnit $unit){
        $ServiceTimeUnitDict = [
            ServiceTimeUnit::DAY => 'DAY',
            ServiceTimeUnit::HOUR => 'HOUR'
        ];

        return $ServiceTimeUnitDict[$unit];
    }

    public static function ServiceUnit(ServiceUnit $unit){
        $ServiceUnitDict = [
            ServiceUnit::G => 'G',
            ServiceUnit::KG => 'KG',
            ServiceUnit::PCS => 'PCS'
        ];

        return $ServiceUnitDict[$unit];
    }
}