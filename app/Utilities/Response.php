<?php
namespace App\Utilities;

class Response{
    public static function ok($data, $message = "Operation done", $status = 200){
        $payload = Array(
            "success"=>True,
            "message"=> $message,
            "data"=>$data,
        );
        return response()->json($payload, $status);
    }

    public static function fail($message, $stack = null, $status = 422){
        $payload = Array(
            "success"=>False,
            "message"=> $message,
            "exception"=>$stack
        );
        return response()->json($payload, $status);
    }
}