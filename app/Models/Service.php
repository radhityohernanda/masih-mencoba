<?php

namespace App\Models;

use App\Traits\TypeAndUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use App\Scopes\TypeScope;

class Service extends Model
{
    use HasFactory, SoftDeletes, Userstamps, TypeAndUuid;

    protected $table = 'm_service';
    protected $keyType = 'uuid';
    public $incrementing = false;

    protected $fillable = ["name", "description", "category_id", "price", "estimation", "time", "unit", "minimum_order", "icon", "merchant_id", "type"];
    protected $hidden = ["type"];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function package()
    {
        return $this->belongsToMany(Package::class, "package_service", "package_id", "service_id");
    }
}
