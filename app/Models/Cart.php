<?php

namespace App\Models;

use App\Enum\OrderType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;
use App\Traits\TypeAndUuid;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Cart extends Model
{
    use HasFactory, Userstamps, TypeAndUuid, SoftDeletes;

    protected $items;

    public $incrementing = false;

    protected $keyType = 'uuid';

    protected $table = 'm_order';

    function __construct($orderType = OrderType::OFFLINE)
    {
        parent::__construct();
        $this->order_type = $orderType;
    }

    function packages(){
        return $this->belongsToMany(Package::class,"order_item", "order_id", "item_id");
    }
    
    function services(){
        return $this->belongsToMany(Service::class,"order_item", "order_id", "item_id");
    }

    function toOrder(){
        $this->type = get_class(new Order());
        $this->save();
        return Order::where('id', $this->id)->first();
    }
}

