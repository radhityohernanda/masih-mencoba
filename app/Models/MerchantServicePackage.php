<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class MerchantServicePackage extends Model
{
    use Uuid, HasFactory, Userstamps, SoftDeletes;

    public $incrementing = false;

    protected $keyType = 'uuid';

    protected $table = 'm_service_package';

    function packages(){
        return $this->belongsTo(Package::class, 'service_id', 'id');
    }
    
    function services(){
        return $this->belongsTo(Service::class,"service_id", 'id');
    }

    function merchant(){
        return $this->belongsTo(Merchant::class, "merchant_id", 'id');
    }
}
