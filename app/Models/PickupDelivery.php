<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use App\Traits\Uuid;

class PickupDelivery extends Model
{
    use Uuid, HasFactory, Userstamps, SoftDeletes;
    protected $guarded = ['id'];
    protected $table = "m_pickup_delivery";
    protected $keyType = 'uuid';
    public $incrementing = false;

    public function merchant()
    {
    return $this->belongsTo('App\Models\Merchant')->withTimestamp();
    }
}
