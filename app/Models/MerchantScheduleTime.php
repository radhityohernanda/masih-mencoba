<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;
use App\Traits\Uuid;

class MerchantScheduleTime extends Model
{
    use Uuid, HasFactory, Userstamps;

    protected $table = 'm_merchant_schedule_time';

    public $incrementing = false;

    protected $keyType = 'uuid';

    public function day(){
        return $this->belongsToMany('App\Models\MerchantScheduleDay', 'merchant_schedule_time_day')->withTimestamps();
    }

    public function merchant(){
        return $this->belongsToMany('App\Models\Merchant', 'merchant_schedule_time_merchant')->withTimestamps();
    }

}
