<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class PaymentProof extends Model
{
    use HasFactory, SoftDeletes, Userstamps, Uuid;
    public $incrementing = false;

    protected $keyType = 'uuid';

    protected $table = 'payment_proof';

    protected $fillable = ['orderId', 'proof_url','created_by', 'updated_by', 'created_at', 'updated_at'];

    public function order(){
        return $this->belongsTo(Order::class, "order_id", "id");
    }
}
