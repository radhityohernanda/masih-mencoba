<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class OrderItem extends Model
{
    use HasFactory, Userstamps;


    protected $primaryKey = null;
    public $incrementing = false;
    protected $table = 'order_item';

    protected $fillable = ['order_id','item_id', 'item_type', 'item_count'];
}
