<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Merchant extends Model
{
    use HasFactory, Userstamps, Uuid, SoftDeletes;

    public $incrementing = false;

    protected $keyType = 'uuid';

    protected $table = 'm_merchant';

    public function user(){
        return $this->belongsToMany('App\Models\User', 'user_merchant')->withTimestamps();
    }

    public function time(){
        return $this->belongsToMany('App\Models\MerchantScheduleTime', 'merchant_schedule_time_merchant')->withTimestamps();
    }

    public function perfume(){
        return $this->hasMany('App\Models\Perfume')->withTimestamps();
    }
    public function payment(){
        return $this->hasMany('App\Models\MerchantPaymentMethod' )->withTimestamps();
    }
    public function pickup(){
        return $this->hasMany('App\Models\PickupDelivery' )->withTimestamps();
    }
    public function categories(){
        return $this->hasMany(Category::class);
    }
}
