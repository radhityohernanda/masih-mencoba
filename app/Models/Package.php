<?php

namespace App\Models;

use App\Traits\TypeAndUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Package extends Model
{
    use HasFactory, SoftDeletes, Userstamps, TypeAndUuid;
    protected $keyType = 'uuid';
    public $incrementing = false;
    public $table = "m_service";
    protected $fillable = ["name", "description", "category_id", "price", "estimation", "time", "unit", "minimum_order", "icon", "merchant_id"];
    protected $hidden = ["type"];
    public function services(){
        return $this->belongsToMany(Service::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

}
