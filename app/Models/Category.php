<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Category extends Model
{
    use HasFactory, SoftDeletes, Userstamps, Uuid;
    protected $table = "m_category";
    protected $keyType = 'uuid';
    public $incrementing = false;


    public function merchant(){
        return $this->belongsTo(Merchant::class);
    }

    public function services(){
        return $this->hasMany(Service::class, 'category_id', 'id');
    }

    public function packages(){
        return $this->hasMany(Package::class, 'category_id', 'id');
    }
}
