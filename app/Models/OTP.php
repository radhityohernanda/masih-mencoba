<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class OTP extends Model
{
    use Uuid, HasFactory;
    protected $table = 'm_otp';

    public $incrementing = false;

    protected $keyType = 'uuid';
}
