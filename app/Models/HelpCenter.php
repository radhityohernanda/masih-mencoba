<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;
use App\Traits\Uuid;

class HelpCenter extends Model
{
    use HasFactory, SoftDeletes , Userstamps, Uuid;

    protected $guarded = ['id'];
    protected $table = 'm_help_center';
    protected $keyType = 'uuid';
    public $incrementing = false;
}
