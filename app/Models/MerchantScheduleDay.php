<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;
use App\Traits\Uuid;

class MerchantScheduleDay extends Model
{
    use Uuid, HasFactory, Userstamps;

    protected $table = 'm_merchant_schedule_day';

    public $incrementing = false;

    protected $keyType = 'uuid';

    public function time(){
        return $this->belongsToMany('App\Models\MerchantScheduleTime', 'merchant_schedule_time_day')->withTimestamps();
    }

}
