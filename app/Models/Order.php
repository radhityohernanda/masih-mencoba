<?php

namespace App\Models;

use App\Traits\TypeAndUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class Order extends Model
{
    use HasFactory, Userstamps, TypeAndUuid;

    public $incrementing = false;

    protected $keyType = 'uuid';

    protected $table = 'm_order';

    function packages(){
        return $this->belongsToMany(Package::class,"order_item", "order_id", "item_id");
    }
    
    function services(){
        return $this->belongsToMany(Service::class,"order_item", "order_id", "item_id");
    }

    public function proofs(){
        return $this->hasMany(PaymentProof::class, "order_id", "id");
    }


}
