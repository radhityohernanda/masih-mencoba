<?php
namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;

class TypeScope implements Scope{
   /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply( \Illuminate\Database\Eloquent\Builder $builder, \Illuminate\Database\Eloquent\Model $model){
      $builder->where('type', '=', get_class($model));
    } 
}
