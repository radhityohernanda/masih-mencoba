<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FailedAuthenticationController extends Controller
{
    public function fail(){
        return response()->json(['logged_in' => 'false'], 401);
    }
}
