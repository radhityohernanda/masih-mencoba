<?php

namespace App\Http\Controllers\Api\Service;

use App\Enum\ServiceTimeUnit;
use App\Enum\ServiceUnit;
use App\Http\Controllers\Controller;
use App\Models\Package;
use Illuminate\Http\Request;
use App\Utilities\Response;
use App\Models\Merchant;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use App\Utilities\FileStorage;

class PackageController extends Controller
{
    public function list(Request $request){
        $limit = $request->input("limit") !== null ? $request->input("limit") : 10;

        if( $request->input("categoryId") !== null){
            $package = Package::where("category_id",$request->input("categoryId"))->paginate($limit);
        }else{
            $package = Package::paginate($limit);
        }
       
        return Response::ok($package);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:200',
            'description' => 'required',
            'categoryId' => 'required',
            'price' => 'required',
            'estimation' => 'required',
            'time' => ['required'],
            'unit' => ['required'],
            'minimumOrder' => 'required',
            'status' => 'nullable|boolean',
            'icon' => 'nullable|base64image|base64max:2048',
            'merchantId' => 'required',
            'serviceIds.*' => 'required|string'
        ]);


        if ($validator->fails()) {
            return Response::fail('invalid input', $validator->errors(), 400);
        }

        $merchant = Merchant::where('id', $request->input('merchantId'))->first();

        if ($merchant == null) {
            return Response::fail('merchant with given id not found', null, 400);
        }

        $category = Category::where('id', $request->input('categoryId'))->first();

        if ($category == null) {
            return Response::fail('category with given id not found', null, 400);
        }

        if($request->input("icon") !== null){
            $url = FileStorage::store($request->input("icon"),"image/icons/service");
        }else{
            $url = null;
        }

        $package = new Package([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'category_id' => $request->input('categoryId'),
            'price' => $request->input('price'),
            'estimation' => $request->input('estimation'),
            'time' => ServiceTimeUnit::tryFrom($request->input("time"))->value,
            'unit' => ServiceUnit::tryFrom($request->input('unit'))->value,
            'minimum_order' => $request->input('minimumOrder'),
            'icon' => $url,
            'merchant_id' => $request->input('merchantId')
        ]);

        if ($request->input('status') !== null) {
            $package->status = $request->input('status');
        }
        $package->save();
        $package->services()->sync($request->input('serviceIds'));
        $package->services;

        return Response::ok($package, 'Package created', 201);
    }

    public function read(Request $request, $id){
        $package = Package::where('id', $id)->first();
        
        if($package == null){
            return Response::fail('Package with given id not found', null, 404);
        }
        $package->services;

        return Response::ok($package);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'nullable|max:200',
            'description' => 'nullable',
            'price' => 'nullable',
            'estimation' => 'nullable',
            'time' => ['nullable'],
            'unit' => ['nullable'],
            'minimumOrder' => 'nullable',
            'status' => 'nullable|boolean',
            'icon' => 'nullable|base64image|base64max:2048',
            'serviceIds.*' => 'nullable|string'
        ]);


        if ($validator->fails()) {
            return Response::fail('invalid input', $validator->errors(), 400);
        }


        $service = Package::where('id', $id)->first();

        if($service == null){
            return Response::fail('Package with given id not found', null, 404);
        }

        if($request->input('name') !== null){
            $service->name = $request->input('name');
        }

        if($request->input('description') !== null){
            $service->description = $request->input('description');
        }

        if($request->input('price') !== null){
            $service->price = $request->input('price');
        }

        if($request->input('estimation') !== null){
            $service->estimation = $request->input('estimation');
        }

        if($request->input('time') !== null){
            $service->time = ServiceTimeUnit::tryFrom($request->input('time'));
        }

        if($request->input('unit') !== null){
            $service->unit = ServiceUnit::tryFrom($request->input('unit'));
        }

        if($request->input('status') !== null){
            $service->status = $request->input('status');
        }

        if($request->input('icon') !== null){
            $url = FileStorage::store($request->input("icon"),"image/icons/service");
            $service->icon = $url;
        }
        
        $service->save();

        if($request->input('serviceIds') !== null){
            $service->services()->sync($request->input('serviceIds'));
        }

        $service->services;


        return Response::ok($service, 'Data updated');
    }

    public function delete(Request $request, $id){
        $service = Package::where('id', $id)->first();
        if($service == null){
            return Response::fail('Package with given id not found', null, 404);
            
        }

        $service->delete();

        return Response::ok($service, 'Service deleted');
    }
}
