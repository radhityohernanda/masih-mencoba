<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Utilities\Response;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'=>'required',
            'password'=>'required'
        ]);

        if($validator->fails()){
            return Response::fail("Invalid input", $validator, 400);
        }

        if (!Auth::attempt($request->only('phone', 'password')))
        {
            return Response::fail("Unauthorized", "wrong phone number or password", 401);
        }

        $user = User::where('phone', $request['phone'])->firstOrFail();

        if(isset($user->verified_at))
        {
            $token = $user->createToken('auth_token')->plainTextToken;

            return Response::ok(['logged_in' => true,'access_token' => $token, 'user_id' => $user->id], "user logged in");
        }

        return Response::ok(['logged_in' => true,'user_id' => $user->id], "user logged in");
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();

        return Response::ok(['logged_in' => false],"user logged out");
    }
}
