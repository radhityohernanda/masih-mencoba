<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Utilities\Response;

class SsoController extends Controller
{
    /**
     * SSO api
     *
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request){
        $validator = Validator::make($request->all(),[
            'email' => 'required|string|email|max:255',
        ]);

        if($validator->fails())
        {
            return Response::fail("invalid input", $validator->errors(), 400);
        }

        $email = $request->input('email');

        $user = User::where('email', $email)->first();

        if(isset($user)){
            $token = $user->createToken('auth_token')->plainTextToken;

            return Response::ok([
                "has_account"=>true,
                "access_token"=> $token
            ], "User logged in");
        }

        return Response::fail("User has no account", null, 404);
    }
}
