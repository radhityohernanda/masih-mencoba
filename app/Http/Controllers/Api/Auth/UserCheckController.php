<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Merchant;
use App\Utilities\Response;
use Carbon\Carbon;
use App\Models\MerchantScheduleDay as Day;

class UserCheckController extends Controller
{
    /**
     * Check if user exist by phone number api
     *
     * @return \Illuminate\Http\Response
     */

    public function userCheck(Request $request){
        $validator = Validator::make($request->all(),[
            'phone' => 'required|string|min:8|max:15',
        ]);

        if($validator->fails())
        {
            return Response::fail("invalid input", $validator->errors(), 400);
        }

        $phone = $request->input('phone');

        $userCheck = User::where('phone', $phone)->first();

        if(isset($userCheck)){
            return Response::ok([
                "has_account"=>true
            ], "account found");
        }

        return Response::fail("User not found", null, 404);
    }

    /**
     * Check if user exist by email api
     *
     * @return \Illuminate\Http\Response
     */

    public function userCheckByEmail(Request $request){

        $validator = Validator::make($request->all(),[
            'email' => 'required|string|email|max:255',
        ]);

        if($validator->fails())
        {
            return Response::fail("invalid input", $validator->errors(), 400);
        }

        $email = $request->input('email');

        $user = User::where('email', $email)->first();

        if(isset($user)){
            return Response::ok([
                "has_account"=>true,
                "email"=>$user->email,
                "uid"=>$user->id
            ], "account found");
        }

        return Response::fail("User not found", null, 404);
    }

}
