<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\EmailSender;
use App\Mail\VerificationEmail;
use App\Models\OTP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Utilities\Response;
use Exception;
use Illuminate\Support\Facades\DB as DB;

class RegisterController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:m_user',
                'phone' => 'required|string|min:8|max:15|unique:m_user',
                'password' => 'required|string|min:8',
                'confirm_password' => 'required|string|min:8|same:password'
            ]);

            if ($validator->fails()) {
                return Response::fail("invalid input", $validator->errors(), 400);
            }

            $user = new User();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->phone = $request->input('phone');
            $user->password = Hash::make($request->input('password'));
            $user->save();

            $otp = new OTP();
            $otp->user_id = $user->id;
            $otp->otp = random_int(10000, 99999);
            $otp->action = 'register_verification';
            $otp->available_until = strtotime("+15 min");
            $otp->save();

            DB::commit();
            $args = Array(
                "email"=>$request->input('email'),
                "content"=> new VerificationEmail($user)
            );
            EmailSender::dispatch($args);

            return Response::ok(['user_created' => true, 'uid' => $user->id], "user created", 201);
        } catch (\Throwable $th) {
            DB::rollBack();
            return Response::fail("unable to create user", $th);
        }
    }

    public function resendOtp(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'user_id' => 'required'
            ]);

            if ($validator->fails()) {
                return Response::fail("invalid input", $validator->errors(), 400);
            }

            $user = User::where('id', $request->input('user_id'))->first();

            if ($user == null) {
                return Response::fail('user not found');
            }
            $args = array(
                "email" => $user->email,
                "content" => new VerificationEmail($user)
            );
            EmailSender::dispatch($args);

            return Response::ok(['uid'=>$user->id, 'email'=>$user->email], "Sending email to ". $user->email);
        }catch(Exception $e){
            return Response::fail('Internal server error', $e, 500);
        }
    }
}
