<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ForgotPassword;
use App\Models\User;
use App\Utilities\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Jobs\EmailSender;

class PasswordController extends Controller
{
    public function requestOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::fail("invalid input", $validator->errors(), 400);
        }

        $user = User::where('email', $request->input('email'))->first();

        if ($user == null) {
            return Response::fail("User with given email not found!", null, 404);
        }

        $args = array(
            "email" => $user->email,
            "content" => new ForgotPassword($user)
        );
        EmailSender::dispatch($args);

        return Response::ok(['uid' => $user->id, 'email' => $user->email], "Sending email to " . $user->email);
    }
}
