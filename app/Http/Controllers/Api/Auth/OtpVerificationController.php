<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\OTP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Utilities\Response;
use Illuminate\Support\Facades\DB;
use \Exception;
use Illuminate\Support\Facades\Hash;

class OtpVerificationController extends Controller
{
    /**
     * OTP verify api
     *
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required|max:5',
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::fail("invalid input", $validator->errors(), 400);
        }
        try {
            DB::beginTransaction();

            $otp = OTP::where('user_id', $request->input('user_id'))
                ->where('otp', $request->input('otp'))
                ->orderBy('created_at','DESC')
                ->first();

            if ($otp == null || $otp->action != 'register_verification') {
                return Response::fail('invalid otp', null, 404);
            }

            if($otp->available_until < strtotime("now")){
                $otp->delete();
                DB::commit();
                return Response::fail('OTP expired');
            }

            $user = User::where('id', $request->input('user_id'))->first();

            if ($user == null) {
                return Response::fail('User not found!', null, 404);
            }

            $user->markEmailAsVerified();
            $token = $user->createToken('auth_token')->plainTextToken;

            $otp->delete();
            DB::commit();
            return Response::ok(['account_verified' => true, 'access_token' => $token], "account verified");
        } catch (Exception $e) {
            DB::rollBack();
            return Response::fail('Unable to verify user', $e);
        }
    }

    public function reset(Request $request){
        $validator = Validator::make($request->all(), [
            'otp' => 'required|max:5',
            'user_id' => 'required',
            'password'=>'required|confirmed'
        ]);

        if ($validator->fails()) {
            return Response::fail("invalid input", $validator->errors(), 400);
        }
        try {
            DB::beginTransaction();

            $otp = OTP::where('user_id', $request->input('user_id'))
                ->where('otp', $request->input('otp'))
                ->orderBy('created_at','DESC')
                ->first();

            if ($otp == null || $otp->action != 'forgot_password') {
                return Response::fail('invalid otp', null, 404);
            }

            if($otp->available_until < strtotime("now")){
                $otp->delete();
                DB::commit();
                return Response::fail('OTP expired');
            }

            $user = User::where('id', $request->input('user_id'))->first();

            if ($user == null) {
                return Response::fail('User not found!', null, 404);
            }

            $user->password = Hash::make($request->input('password'));
            $user->save();

            $otp->delete();
            DB::commit();
            return Response::ok(['password_reset' => true,], "password reset");
        } catch (Exception $e) {
            DB::rollBack();
            return Response::fail('Unable to reset password', $e);
        }
    }
}
