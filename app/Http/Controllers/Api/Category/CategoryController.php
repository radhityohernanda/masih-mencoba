<?php

namespace App\Http\Controllers\Api\Category;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Merchant;
use App\Utilities\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Utilities\FileStorage;

class CategoryController extends Controller
{
    public function list(Request $request)
    {
        $limit = $request->input("limit") !== null ? $request->input("limit") : 10;
        if($request->input("merchantId") !== null){
            $categories = Category::where("merchant_id",'=', $request->input("merchantId"))
            ->with('services')
            ->with('packages', 'packages.services')
            ->paginate($limit);
        }else{
            $categories = Category::paginate($limit);
        }
        return Response::ok($categories);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'icon' => 'nullable|base64image|base64max:2048',
            'merchantId' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::fail('invalid input', $validator->errors(), 400);
        }

        $merchant = Merchant::where('id', $request->input('merchantId'))->first();

        if (!isset($merchant)) {
            return Response::fail('merchant with given id not found', null, 400);
        }

        if($request->input("icon") !== null){
            $url = FileStorage::store($request->input("icon"),"image/icons/service");
        }else{
            $url = "";
        }

        $category = new Category();
        $category->name = $request->input('name');
        $category->icon = $url;
        $category->merchant_id = $request->input('merchantId');
        $category->save();

        return Response::ok($category,"Category Created",201);
    }

    public function read(Request $request, $id)
    {
        $category = Category::where('id', $id)->first();

        if ($category == null) {
            return Response::fail("Category with given id " . $id . " was not found!", null, 404);
        }

        return Response::ok($category);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'max:100',
            'icon' => 'nullable|base64image|base64max:2048',
        ]);

        if ($validator->fails()) {
            return Response::fail('invalid input', $validator->errors(), 400);
        }

        $category = Category::where('id', $id)->first();

        if ($category == null) {
            return Response::fail("Category with given id " . $id . " was not found!", null, 400);
        }

        if ($request->input('name') !== null) {
            $category->name = $request->input('name');
        }

        if ($request->input('icon') !== null) {
            $url = FileStorage::store($request->input("icon"),"image/icons/service");
            $category->icon = $url;
        }

        $category->save();

        return Response::ok($category, "Category updated");
    }

    public function delete(Request $request, $id)
    {
        $category = Category::where('id', $id)->first();

        if ($category == null) {
            return Response::fail("Category with given id " . $id . " was not found!", null, 400);
        }

        $category->delete();

        return Response::ok($category, "Category deleted successfully");
    }
}
