<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\DB as DB;
use App\Models\Address;
use App\Models\User;
use App\Utilities\Response;
use Illuminate\Support\Facades\Validator;


class AddressController extends Controller
{
    public function list(Request $request)
    {
        $limit = $request->input("limit") !== null ? $request->input("limit") : 10;
        if($request->input("user_id") !== null){
            $address = Address::where("user_id",'=', $request->input("user_id"))
            ->paginate($limit);
        }else{
            $address = Address::paginate($limit);
        }
        return Response::ok($address);
    }
    
    public function get( $id)
    {
        $address = Address::find($id);
        if (is_null($address)) {
            return Response::fail("address not found",null,404);
        }
        return Response::ok($address, 'successfully getting user address',200);
    }
    
    public function create(Request $request)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(),[
                'address' => 'required',
                'sub_district' => 'required',
                'district' => 'required',
                'province' => 'required',
                'postal_code' => 'required',
                'user_id' => 'required',
                'longitude' => 'nullable',
                'latitude' => 'nullable'
            ]);
            if($validator->fails()){
                return Response::fail('invalid input',$validator->errors(),400);
            };
            $address = new Address();
            $address->address = $request->input('address');
            $address->sub_district = $request->input('sub_district');
            $address->district = $request->input('district');
            $address->province = $request->input('province');
            $address->postal_code = $request->input('postal_code');
            $address->user_id = $request->input('user_id');
            $address->longitude = $request->input('longitude');
            $address->latitude = $request->input('latitude');
            $address->save();
            DB::commit();
            return Response::ok($address, 'user address created succesfully',201);
        }catch (\Throwable $th){
            DB::rollBack();
            return Response::fail("create address failed", $th->getMessage(), 422);
        }
    }
    
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(),[
                'address' => 'required',
                'sub_district' => 'required',
                'district' => 'required',
                'province' => 'required',
                'postal_code' => 'required',
            ]);
            if($validator->fails()){
                return Response::fail('invalid input',$validator->errors(),400);
            }
            $address = new Address();
            $address->address = $request->input('address');
            $address->sub_district = $request->input('sub_district');
            $address->district = $request->input('district');
            $address->province = $request->input('province');
            $address->postal_code = $request->input('postal_code');
            $address->save();
            DB::commit();
                return Response::ok($address, 'address updated successfully',200);
        }catch (\Throwable $th){
            DB::rollBack();
            return Response::fail("address update failed", $th->getMessage(), 422);
        }
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $address = Address::where('id', $id)->first();
            if ($address == null) {
                return Response::fail("user address with given id " . $id . " was not found!", null, 404);
            }
            $address->delete();
            DB::commit();
                return Response::ok($address, "user address deleted successfully", 200);
        }catch (\throwable $th){
            DB::rollBack();
            return Response::fail("user address delete failed", $th->getMessage(), 422);

        }
    }
}