<?php

namespace App\Http\Controllers\API; 

use App\Models\HelpCenter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Utilities\Response;
use Illuminate\Support\Facades\DB as DB;

class HelpCenterController extends Controller  
{
    public function getAll()
    {
            $help = HelpCenter::all();
            if (is_null($help)){
                return Response::fail('data not found',null,404);
            }
            return response::ok($help, 'get data successfully');        
            
    }

    public function create(Request $request)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(),[
                'email' => 'required|email',
                'phone' => 'required'
            ]);
            if($validator->fails()){
                return Response::fail('Failed',$validator->errors(),400);
            };
      
            $help = new HelpCenter();
            $help->email = $request->input('email');
            $help->phone = $request->input('phone');
            $help->save();

            DB::commit();
            return Response::ok($help, 'data created', 201);
        
        } catch(\Throwable $th){
            DB::rollBack();
            return Response::fail('create data failed', $th->getMessage(),422);
        }
    }


    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(),[
                'email' => 'required|email',
                'phone' => 'required',
            ]);
            if($validator->fails()){
                return Response::fail('Failed',$validator->errors(),400);
            }else{
                $help = HelpCenter::find($id);
                $help->email = $request->input('email');
                $help->phone = $request->input('phone');
                $help->save();
                DB::commit();
                return Response::ok($help, "data updated", 201);
            }
        } catch(\Throwable $th){
            DB::rollBack();
            return Response::fail('update data failed', $th->getMessage() ,422);
        }
        
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $help = HelpCenter::find($id);
            $help->delete();

            DB::commit();
            return Response::ok($help , "Delete data successfully");
        } catch (\Throwable $th){
            DB::rollBack();
            return Response::fail('delete data failed',$th->getMessage());
        }
    }
}