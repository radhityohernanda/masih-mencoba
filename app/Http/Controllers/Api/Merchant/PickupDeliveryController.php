<?php

namespace App\Http\Controllers\Api\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\DB as DB;
use App\Models\PickupDelivery;
use App\Models\Merchant;
use App\Utilities\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rules\Enum;
use App\Utilities\FileStorage;

class PickupDeliveryController extends Controller
{
    public function list(Request $request)
    {
        $limit = $request->input("limit") !== null ? $request->input("limit") : 10;
        if($request->input("merchant_id") !== null){
            $pickup = PickupDelivery::where("merchant_id",'=', $request->input("merchant_id"))
            ->paginate($limit);
        }else{
            $pickup = PickupDelivery::paginate($limit);
        }
        return Response::ok($pickup);
    }
    

    public function get($id)
    {
        $pickup = PickupDelivery::find($id);
        if (is_null($pickup)) {
            return Response::fail("pickup and delivery data not found",null,404);
        }
        return Response::ok($pickup, 'successfully getting pickup and delivery data',200);
    }
    
    public function create(Request $request)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(),[
                'type' => 'required|in:OFFLINE,ONLINE',
                'max_distance' => 'required',
                'distance' => 'required',
                'shipping_charges' => 'required',
                'merchant_id' => 'required',
            ]);
            if($validator->fails()){
                return Response::fail('invalid input',$validator->errors(),400);
            };
            $pickup = new PickupDelivery();
            $pickup->type = $request->input('type');
            $pickup->max_distance = $request->input('max_distance');
            $pickup->distance = $request->input('distance');
            $pickup->shipping_charges = $request->input('shipping_charges');
            $pickup->merchant_id = $request->input('merchant_id');
            $pickup->save();

            DB::commit();
            return Response::ok($pickup, 'pickup and delivery data created succesfully',201);
        }catch (\Throwable $th){
            DB::rollBack();
            return Response::fail("create pickup and delivery data failed", $th->getMessage(), 422);
        }
    }
    
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(),[
                'type' => 'required|in:OFFLINE,ONLINE',
                'max_distance' => 'required',
                'distance' => 'required',
                'shipping_charges' => 'required',
            ]);
        if($validator->fails()){
            return Response::fail('invaild input',$validator->errors(),400);
        }else{
        $pickup = PickupDelivery::find($id);
        $pickup->type = $request->input('type');
        $pickup->max_distance = $request->input('max_distance');
        $pickup->distance = $request->input('distance');
        $pickup->shipping_charges = $request->input('shipping_charges');
        $pickup->save();

        DB::commit();
            return Response::ok($pickup, 'pickup and delivery data updated successfully',200);
        }
        }catch (\Throwable $th){
            DB::rollBack();
            return Response::fail("pickup and delivery data update failed", $th->getMessage(), 422);
        }
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $pickup = Pickupdelivery::where('id', $id)->first();
            if ($pickup == null) {
                return Response::fail("pickup and delivery with given id " . $id . " was not found!", null, 404);
            }
            $pickup->delete();
            DB::commit();
                return Response::ok($pickup, "pickup and delivery data deleted successfully", 200);
        }catch (\throwable $th){
            DB::rollBack();
            return Response::fail("pickup and delivery data delete failed", $th->getMessage(), 422);

        }
    }

}