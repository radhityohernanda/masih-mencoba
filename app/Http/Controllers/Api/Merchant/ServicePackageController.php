<?php

namespace App\Http\Controllers\Api\Merchant;

use App\Http\Controllers\Controller as Controller;
use App\Models\Merchant;
use App\Models\MerchantServicePackage;
use App\Utilities\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class ServicePackageController extends Controller
{
    public function list(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'merchantId' => 'required',
        ]);
        if ($validator->fails()) {
            return Response::fail('invalid input', $validator->errors(), 400);
        }

        $limit = $request->input("limit") ? $request->input("limit") : 10;
        return MerchantServicePackage::where('merchant_id', $request->input('merchantId'))
            ->with('packages', 'packages.services', 'services')
            ->paginate($limit);
    }
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'merchantId' => 'required',
            'serviceId' => 'required',
            'price' => 'required',
            'amount' => 'required'
        ]);
        if ($validator->fails()) {
            return Response::fail('invalid input', $validator->errors(), 400);
        }

        $merchant = Merchant::where('id', $request->input('merchantId'))->first();

        if ($merchant == null) {
            return Response::fail('merchant not found');
        }

        $service = DB::table('m_service')
            ->select(['*'])
            ->where('id', '=', $request->input('serviceId'))
            ->first();

        if ($service == null) {
            return Response::fail('service not found');
        }

        $package = new MerchantServicePackage();
        $package->service_id = $service->id;
        $package->merchant_id = $merchant->id;
        $package->amount = $request->input('amount');
        $package->price = $request->input('price');


        $package->save();

        return Response::ok($package, 'service package created', 201);
    }
    public function read(Request $request, $id)
    {
        $package = MerchantServicePackage::with('packages', 'packages.services', 'services', 'merchant')
            ->where('id', $id)->first();
        if ($package == null) {
            return Response::fail('service package not found');
        }

        return Response::ok($package);
    }
    // public function update(Request $request, $id){}
    public function delete(Request $request, $id)
    {
        $package = MerchantServicePackage::where('id', $id)->first();

        if ($package == null) {
            return Response::fail('service package not found');
        }

        $package->delete();
        return Response::ok($package, 'service package deleted');
    }
}
