<?php

namespace App\Http\Controllers\API\Merchant; 

use App\Models\Perfume;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Utilities\Response;
use Illuminate\Support\Facades\DB as DB;

class PerfumeController extends Controller  
{
    public function get($id)
    {
            $perfume = Perfume::find($id);
            if (is_null($perfume)){
                return Response::fail('data perfume not found',null,404);
            }
            return response::ok($perfume, 'get data perfume successfully');        
    }

    public function list(Request $request)
    {
        {
            $limit = $request->input("limit") !== null ? $request->input("limit") : 10;
            if($request->input("merchant_id") !== null){
                $perfume = Perfume::where("merchant_id",'=', $request->input("merchant_id"))->paginate($limit);
            }else{
                $perfume = Perfume::paginate($limit);
            }
            return Response::ok($perfume);
        }
    }

    public function create(Request $request)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'is_available' => 'required',
                'merchant_id' => 'required'
            ]);
            if($validator->fails()){
                return Response::fail('Failed',$validator->errors(),400);
            };
      
            $perfume = new Perfume();
            $perfume->name = $request->input('name');
            $perfume->is_available = $request->input('is_available');
            $perfume->merchant_id = $request->input('merchant_id');
            $perfume->save();

            DB::commit();
            return Response::ok($perfume, 'perfume created', 201);
        
        } catch(\Throwable $th){
            DB::rollBack();
            return Response::fail('create perfume failed', $th->getMessage(),422);
        }
    }


    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'is_available' => 'required'

            ]);
            if($validator->fails()){
                return Response::fail('Failed',$validator->errors(),400);
            }else{
                $perfume = Perfume::find($id);
                $perfume->name = $request->input('name');
                $perfume->is_available = $request->input('is_available');
                $perfume->save();
                DB::commit();
                return Response::ok($perfume, "perfume updated", 200);
            }
        } catch(\Throwable $th){
            DB::rollBack();
            return Response::fail('update perfume failed', $th->getMessage() ,422);
        }
        
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $perfume = Perfume::find($id);
            $perfume->delete();

            DB::commit();
            return Response::ok($perfume , "Delete perfume data successfully");
        } catch (\Throwable $th){
            DB::rollBack();
            return Response::fail('delete perfume data failed',$th->getMessage());
            
        }
    }
}