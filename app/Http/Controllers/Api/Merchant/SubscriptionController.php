<?php

namespace App\Http\Controllers\Api\Merchant;

use App\Enum\PaymentStatus;
use App\Exceptions\CoreException;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Services\Midtrans\CreateSnapTokenService;
use App\Models\Order;
use App\Models\Subscription;
use App\Models\User;
use App\Models\OrderItem;
use App\Utilities\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Services\Midtrans\CallbackService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Models\Merchant;

class SubscriptionController extends Controller
{
    /**
     * get all subscription data api
     *
     * @return Merchant
     */
    public function get()
    {
        $subscription = Subscription::all();

        if($subscription){
            return response::ok(['subscription' => $subscription],  'get data subscription successfully');
        }

        return response::fail('subscription not found', null, 404);

    }

    public function order(Request $request, $id){
        try{
            $validator = Validator::make($request->all(),[
                'total_price' => 'required',
                'name' => 'required',
                'total_item' => 'required'
            ]);

            if($validator->fails())
            {
                return Response::fail('invalid input', $validator->errors(), 400);
            }

            $user = User::find(Auth::id());

            $order = new Order();
            $order->total_price = $request->input('total_price');
            $order->payment_status = 1;
            $order->user_id = Auth::id();
            $order->merchant_id = $id;
            $order->save();

            $subscription = Subscription::where('name', $request->input('name'))->first();


            $order_item = new OrderItem();
            $order_item->order_id = $order->id;
            $order_item->item_id = $subscription->id;
            $order_item->item_type = get_class($subscription);
            $order_item->item_count = $request->input('total_item');
            $order_item->save();

            $midtrans = new CreateSnapTokenService($order, $user);
            $snap_token = $midtrans->getSnapToken();

            return response::ok(['snap_token' => $snap_token],  'create order successfully',201);

        } catch(\Throwable $th){
            return response::fail('failed to create order', $th->getMessage());
        }
    }

    public function receive()
    {
        $callback = new CallbackService;
        DB::beginTransaction();
        try{
            if ($callback->isSignatureKeyVerified()) {
                $notification = $callback->getNotification();
                $order_data = $callback->getOrder();
                $order = Order::find($order_data->id)->lockForUpdate();
    
                if ($callback->isSuccess()) {
                    $order->payment_status = 2;
                    $order->save();
    
                    $merchant = Merchant::find($order->merchant_id);
    
                    $order_item = OrderItem::where('order_id', $order->id)->lockForUpdate()->first();
                    $subscription = Subscription::find($order_item->item_id);
    
                    switch ($subscription->type){
                        case "day":
                            $merchant->subscription_end_date = Carbon::now()->addDays($subscription->type_total);
                            $merchant->save();
                            break;
                        case "month":
                            $merchant->subscription_end_date = Carbon::now()->addMonths($subscription->type_total);
                            $merchant->save();
                            break;
                        case "year":
                            $merchant->subscription_end_date = Carbon::now()->addYears($subscription->type_total);
                            $merchant->save();
                            break;
                    }
                }
    
                if ($callback->isExpire()) {
                    $order->payment_status = PaymentStatus::EXPIRED;
                    $order->save();
                }
    
                if ($callback->isCancelled()) {
                    $order->payment_status = PaymentStatus::CANCEL;
                    $order->save();
                }
                DB::commit();
                return response::ok(['order_status' => $order->payment_status],  'notification created successfully');
            } else {
                throw new CoreException("Unable to verify signature", 422);
            }
        } catch (CoreException $th) {
            DB::rollBack();
            return Response::fail($th->getMessage(), $th->getDetails(), $th->getStatus());
        } catch (Exception $th) {
            DB::rollBack();
            return Response::fail($th->getMessage(), null, 500);
        }
    }


    public function allOrder($id)
    {
        $orders = Order::where('merchant_id', $id)->get();

        if($orders){
            return response::ok($orders, 'get data orders successfully');
        }

        return response::fail('orders not found', null, 404);
    }
}
