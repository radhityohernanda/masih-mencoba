<?php

namespace App\Http\Controllers\Api\Merchant;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\Merchant;
use App\Models\User;
use App\Models\MerchantScheduleTime as Time;
use App\Models\MerchantScheduleDay as Day;
use App\Utilities\Response;
use Illuminate\Support\Facades\DB as DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use App\Jobs\OpenMerchant;
use App\Jobs\CloseMerchant;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Utilities\FileStorage;

class MerchantController extends Controller
{
     /**
     * get merchant data api
     *
     * @return Merchant
     */
    public function get($id)
    {
        try{
            $merchant = Merchant::whereHas('user', function($query) use($id){
                $query->where('m_user.id', Auth::id())
                ->where('m_merchant.id', $id);
            })->get();

            $merchantSchedule = Day::join('merchant_schedule_time_day', 'merchant_schedule_time_day.merchant_schedule_day_id', 'm_merchant_schedule_day.id')
                                    ->join('m_merchant_schedule_time', 'm_merchant_schedule_time.id', 'merchant_schedule_time_day.merchant_schedule_time_id')
                                    ->join('merchant_schedule_time_merchant', 'merchant_schedule_time_merchant.merchant_schedule_time_id', 'm_merchant_schedule_time.id')
                                    ->join('m_merchant', 'm_merchant.id', 'merchant_schedule_time_merchant.merchant_id')
                                    ->where('m_merchant.id',$id)
                                    ->get(['m_merchant_schedule_day.day','m_merchant_schedule_time.open_at','m_merchant_schedule_time.close_at','m_merchant_schedule_time.is_24hours']);

            return response::ok(['merchant' => $merchant, 'schedule' => $merchantSchedule], 'get data merchant successfully');
        } catch(\Throwable $th){
            return response::fail('merchant not found', null, 404);
        }
    }

    /**
     * get all merchant data by user relationships api
     *
     * @return Merchant
     */
    public function getAll()
    {
        try{
            $user = User::find(Auth::id());
            $merchants = $user->merchant()->get();

            $merchantSchedule = Day::join('merchant_schedule_time_day', 'merchant_schedule_time_day.merchant_schedule_day_id', 'm_merchant_schedule_day.id')
                                    ->join('m_merchant_schedule_time', 'm_merchant_schedule_time.id', 'merchant_schedule_time_day.merchant_schedule_time_id')
                                    ->join('merchant_schedule_time_merchant', 'merchant_schedule_time_merchant.merchant_schedule_time_id', 'm_merchant_schedule_time.id')
                                    ->join('m_merchant', 'm_merchant.id', 'merchant_schedule_time_merchant.merchant_id')
                                    ->join('user_merchant', 'user_merchant.merchant_id', 'm_merchant.id')
                                    ->join('m_user', 'm_user.id', 'user_merchant.user_id')
                                    ->where('m_user.id',Auth::id())
                                    ->get(['m_merchant.id as merchant_id','m_merchant_schedule_day.day','m_merchant_schedule_time.open_at','m_merchant_schedule_time.close_at','m_merchant_schedule_time.is_24hours']);

            return response::ok(['merchants' => $merchants, 'schedule' => $merchantSchedule],  'get data merchants successfully');
        } catch(\Throwable $th){
            return response::fail('merchant not found', null, 404);
        }
    }

    /**
     * get all merchant parent data api
     *
     * @return Merchant
     */
    public function getAllParent()
    {
        try{
            $merchants = Merchant::where('parent_id',null)->get();

            return response::ok(['merchants' => $merchants],  'get data merchants successfully');
        } catch(\Throwable $th){
            return response::fail('merchant not found', null, 404);
        }
    }

    public function create(Request $request, $parentId = null)
    {
        DB::beginTransaction();
        try {

            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'image' => 'nullable|base64image|base64max:2048',
                'bank_name' => 'nullable',
                'bank_account_number' => 'nullable',
                'longitude' => 'nullable',
                'latitude' => 'nullable'
            ]);

            if($validator->fails())
            {
                return Response::fail('invalid input', $validator->errors(), 400);
            }

            $merchant = new Merchant();
            $merchant->name = $request->input('name');
            $merchant->address = $request->input('address');
            $merchant->phone = $request->input('phone');
            $merchant->bank_name = $request->input('bank_name');
            $merchant->bank_account_number = $request->input('bank_account_number');
            $merchant->longitude = $request->input('longitude');
            $merchant->latitude = $request->input('latitude');

            if(isset($parentId)){
                $merchant->parent_id = $parentId;
            }

            if($request->input('image')){
                $url = FileStorage::store($request->input("image"),"image/icons/merchant");

                $merchant->image = $url;
            }

            $merchant->save();

            $merchant->user()->attach([Auth::id()]);

            $days = Day::whereRaw("day IN ('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu')")->get();

            // create time for days schedule
            for($index = 0; $index < sizeof($days) ; $index++){
                $time = new Time();
                // attach to merchant id
                // can be improved
                $time->save();
                $time->merchant()->attach([$merchant->id]);
                $time->day()->attach([$days[$index]->id]);
            }


            $merchantSchedule = Day::join('merchant_schedule_time_day', 'merchant_schedule_time_day.merchant_schedule_day_id', 'm_merchant_schedule_day.id')
                                    ->join('m_merchant_schedule_time', 'm_merchant_schedule_time.id', 'merchant_schedule_time_day.merchant_schedule_time_id')
                                    ->join('merchant_schedule_time_merchant', 'merchant_schedule_time_merchant.merchant_schedule_time_id', 'm_merchant_schedule_time.id')
                                    ->join('m_merchant', 'm_merchant.id', 'merchant_schedule_time_merchant.merchant_id')
                                    ->where('m_merchant.id',$merchant->id)
                                    ->get(['m_merchant_schedule_day.day','m_merchant_schedule_time.open_at','m_merchant_schedule_time.close_at','m_merchant_schedule_time.is_24hours']);
            DB::commit();

            return Response::ok(['merchant' => $merchant, 'schedule' => $merchantSchedule], "merchant created", 201);
        } catch(\Throwable $th){
            DB::rollBack();
            return Response::fail('create merchant failed', $th->getMessage());
        }
    }

    public function update(Request $request, $id){
        try {
            DB::beginTransaction();
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'image' => 'nullable|base64image|base64max:2048',
                'bank_name' => 'nullable',
                'bank_account_number' => 'nullable',
                'longitude' => 'nullable',
                'latitude' => 'nullable'
            ]);

            if($validator->fails())
            {
                return Response::fail('invalid input', $validator->errors(), 400);
            }

            $merchant = Merchant::find($id);
            $merchant->name = $request->input('name');
            $merchant->address = $request->input('address');
            $merchant->phone = $request->input('phone');
            $merchant->bank_name = $request->input('bank_name');
            $merchant->bank_account_number = $request->input('bank_account_number');
            $merchant->longitude = $request->input('longitude');
            $merchant->latitude = $request->input('latitude');

            if($request->input('image')){
                if($merchant->image !== null){
                    FileStorage::delete($merchant->image);
                }

                $url = FileStorage::store($request->input("image"),"image/icons/merchant");

                $merchant->image = $url;
            }

            if($request->has('delete_photo')){
                FileStorage::delete($merchant->image);

                $merchant->image = null;
            }

            $merchant->save();

            DB::commit();

            return Response::ok($merchant, "merchant updated");
        } catch(\Throwable $th){
            DB::rollBack();
            return Response::fail('update merchant profile failed');
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();

            $merchant = Merchant::find($id);

            if($merchant->image !== null){
                FileStorage::delete($merchant->image);
            }

            $merchant->delete();

            $merchant->user()->detach();

            DB::commit();

            return Response::ok($merchant,"Delete merchant data successfully");
        } catch (\Throwable $th){
            DB::rollBack();
            return Response::fail('delete merchant profile failed');
        }

    }

    public function changeStatus(Request $request, $id){
        DB::beginTransaction();
        try{

            $validator = Validator::make($request->all(),[
                'is_open' => 'required',
                'time_start' => 'nullable',
                'time_end' => 'nullable'
            ]);

            if($validator->fails())
            {
                return Response::fail('invalid input', $validator->errors(), 400);
            }

            $merchant = Merchant::find($id);

            if($request->input('time_start') && $request->input('time_end')){
                $merchant->time_start = $request->input('time_start');
                $merchant->time_end = $request->input('time_end');
                $merchant->save();

                $time_now = Carbon::now();
                $time_start = Carbon::parse($request->input('time_start'));
                $time_end = Carbon::parse($request->input('time_end'));

                $diffClose = $time_now->diffInSeconds($time_start);
                $diffOpen = $time_now->diffInSeconds($time_end);

                CloseMerchant::dispatch($merchant)->delay($diffClose);
                OpenMerchant::dispatch($merchant)->delay($diffOpen);

                DB::commit();

                return Response::ok(['status' => "OK"], "Merchant status updated successfully");
            } else {
                $is_open = $request->input('is_open');

                $merchant->is_open = $is_open;
                $merchant->save();

                DB::commit();

                return Response::ok(['is_open' => $is_open], "Merchant status updated successfully");
            }

        } catch (\Throwable $th){
            DB::rollBack();
            return Response::fail('Merchant status update failed');
        }
    }

    public function updateSchedule(Request $request, $id){
        DB::beginTransaction();
        try{

            $validator = Validator::make($request->all(),[
                'day' => 'required',
                'is_close' => 'required',
                'is_24_hours' => 'required',
                'open_at' => 'nullable',
                'close_at' => 'nullable',
            ]);

            if($validator->fails())
            {
                return Response::fail('invalid input', $validator->errors(), 400);
            }

            $day = $request->input('day');
            $is24hours = $request->input('is_24_hours');
            $isClose = $request->input('is_close');
            $openAt = $request->input('open_at') ? $request->input('open_at') : null ;
            $closeAt = $request->input('close_at') ? $request->input('close_at') : null ;

            $schedule = Time::whereHas('merchant', function($query) use($id){
                $query->where('m_merchant.id', $id);
            })->whereHas('day', function($query2) use($day){
                $query2->where('m_merchant_schedule_day.day', $day);
            })->first();

            if(!$isClose){
                if($is24hours){
                    $schedule->open_at = null;
                    $schedule->close_at = null;
                    $schedule->is_24hours = true;
                    $schedule->save();
                }else{
                    $schedule->open_at = $openAt;
                    $schedule->close_at = $closeAt;
                    $schedule->is_24hours = false;
                    $schedule->save();
                }
            }else{
                $schedule->open_at = null;
                $schedule->close_at = null;
                $schedule->is_24hours = null;
                $schedule->save();
            }

            DB::commit();

            return Response::ok(['schedule' => $schedule], "Merchant schedule updated successfully");
         } catch(\Throwable $th){
            DB::rollBack();
            return Response::fail('Merchant schedule update failed');
        }
    }
}
