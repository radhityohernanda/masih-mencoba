<?php

namespace App\Http\Controllers\Api\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\DB as DB;
use App\Models\MerchantPaymentMethod;
use App\Utilities\Response;
use Illuminate\Support\Facades\Validator;


class PaymentMethodController extends Controller
{
    public function list(Request $request)
    {
        $limit = $request->input("limit") !== null ? $request->input("limit") : 10;
        if($request->input("merchantId") !== null){
            $payment = MerchantPaymentMethod::where("merchant_id",'=', $request->input("merchantId"))
            ->paginate($limit);
        }else{
            $payment = MerchantPaymentMethod::paginate($limit);
        }
        return Response::ok($payment);
    }
    

    public function get( $id)
    {
        $payment = MerchantPaymentMethod::find($id);
        if (is_null($payment)) {
            return Response::fail("payment not found",null,404);
        }
        return Response::ok($payment, 'successfully getting payment method',200);
    }
    
    public function create(Request $request)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(),[
                'type' => 'required|in:BANK,DIGITAL_WALLET,OTHER',
                'name' => 'required',
                'icon' => 'nullable|base64image|base64max:2048',
                'account' => 'required',
                'description' => 'required',
                'merchant_id' => 'required',
            ]);
            if($validator->fails()){
                return Response::fail('invalid input',$validator->errors(),400);
            };
            $payment = new MerchantPaymentMethod();
            $payment->type = $request->input('type');
            $payment->name = $request->input('name');
            $payment->account = $request->input('account');
            $payment->description = $request->input('description');
            $payment->merchant_id = $request->input('merchant_id');

            if($request->input('icon')){
                $url = FileStorage::store($request->input("icon"),"image/icons/pament");
                $payment->icon = $url;
            }

            $payment->save();
            DB::commit();
            return Response::ok($payment, 'payment created succesfully',201);
        }catch (\Throwable $th){
            DB::rollBack();
            return Response::fail("create payment failed", $th->getMessage(), 422);
        }
    }
    
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $validator = Validator::make($request->all(),[
                'type' => 'required|in:BANK,DIGITAL_WALLET,OTHER',
                'name' => 'required|string|max:50',
                'account' => 'nullable|string',
                'description' => 'nullable|string'
            ]);
        if($validator->fails()){
            return Response::fail('invaild input',$validator->errors(),400);
            }
            $payment = MerchantPaymentMethod::find($id);
            $payment->type = $request->input('type');
            $payment->name = $request->input('name');
            $payment->account = $request->input('account');
            $payment->description = $request->input('description');

            if($request->input('icon')){
                $url = FileStorage::store($request->input("icon"),"image/icons/payment");
                $payment->icon = $url;
            }
            $payment->save();
            DB::commit();
            return Response::ok($payment, 'payment updated successfully',200);
        
        }catch (\Throwable $th){
            DB::rollBack();
            return Response::fail("payment update failed", $th->getMessage(), 422);
        }
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $payment = MerchantPaymentMethod::where('id', $id)->first();
            if($payment->icon !== null){
                FileStorage::delete($payment->icon);
            }
            $payment->delete();
            DB::commit();
                return Response::ok($payment, "Payment deleted successfully", 200);
        }catch (\throwable $th){
            DB::rollBack();
            return Response::fail("Payment delete failed", $th->getMessage(), 422);

        }
    }

}