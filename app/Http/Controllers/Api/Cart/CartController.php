<?php

namespace App\Http\Controllers\Api\Cart;

use App\Enum\OrderType;
use App\Enum\PaymentStatus;
use App\Exceptions\CoreException;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Utilities\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Merchant;
use App\Models\OrderItem;
use Exception;
use App\Models\Service;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    function list(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'merchantId' => 'required',
        ]);
        if ($validator->fails()) {
            throw new CoreException('invalid input', 400, $validator->errors());
        }

        $limit = $request->input("limit") ? $request->input("limit") : 10;
        $cart = new Cart(OrderType::OFFLINE);
        $carts = $cart
        ->with("packages", "packages.services", "services")
        ->where('merchant_id', $request->input('merchantId'))
        ->paginate($limit);
        return Response::ok($carts);
    }

    function create(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'merchantId' => 'required',
                'orderType'=>"required|in:OFFLINE, ONLINE",
                'items.*.id' => 'required',
                'userId'=>'nullable',
                'items.*.count' => 'required'
            ]);
            if ($validator->fails()) {
                throw new CoreException('invalid input', 400, $validator->errors());
            }

            $merchant = Merchant::where('id', $request->input('merchantId'))->first();

            if ($merchant == null) {
                throw new CoreException('Merchant with given id not found!', 404);
            }


            $service_ids =  array_map(function($item){
                return $item["id"];
            }, $request->input('items'));

            $services = DB::table("m_service")
                ->select(["id","price", "minimum_order"])
                ->whereIn('id',$service_ids)
                ->get();

            $total = 0;

            foreach ($service_ids as $index => $id) {
                $index = array_search($id, array_column($services->toArray(), 'id'));
                $reqIndex = array_search($id, array_column($request->input('items'), 'id'));

                $req = $request->input('items')[$reqIndex];
                $service = $services[$index];

                $amount = $req["count"] > $service->minimum_order ?$req["count"] :$service->minimum_order;
                $price = $amount * $service->price;

                $total = $total + $price;
            }

            $cart = new Cart(OrderType::OFFLINE);
            $cart->total_price = $total;
            $cart->payment_status = PaymentStatus::CART;

            if($request->input('userId') != null ){
                $cart->user_id = $request->input('userId');
            }

            $cart->merchant_id = $request->input("merchantId");
            $cart->save();

            $data = [];
            $itemType = get_class(new Service()); // Hardcoded for now

            foreach ($request->input('items') as $item) {
                $newData = [
                    "order_id" => $cart->id,
                    "item_id" => $item["id"],
                    "item_type" => $itemType,
                    "item_count" => floatval($item["count"])
                ];

                array_push($data, $newData);
            }
            OrderItem::insert($data);
            DB::commit();
            return Response::ok($cart, "created",201);
        } catch (CoreException $th) {
            DB::rollBack();
            return Response::fail($th->getMessage(), $th->getDetails(), $th->getStatus());
        } catch (Exception $th) {
            DB::rollBack();
            return Response::fail($th->getMessage(), null, 500);
        }
    }

    function read(Request $request, $cartId)
    {
        try {
            $cart = Cart::where('id', $cartId)
            ->with('packages', 'packages.services', 'services')
            ->first();

            if ($cart == null) {
                throw new CoreException("Cart with given id not found!", 404);
            }

            return Response::ok($cart);
        } catch (CoreException $th) {
            return Response::fail($th->getMessage(), $th->getDetails(), $th->getStatus());
        } catch (Exception $th) {
            return Response::fail($th->getMessage(), null, 500);
        }
    }

    function update(Request $request, $cartId)
    {
        $validator = Validator::make($request->all(), [
            'items.*.id' => 'nullable',
            'items.*.count' => 'nullable',
            'userId'=>'nullable'
        ]);
        if ($validator->fails()) {
            throw new CoreException('invalid input', 400, $validator->errors());
        }

        $cart = Cart::where('id', $cartId)->first();

        if($request->input('items') !== null){
            $data = [];
            $itemType = get_class(new Service()); // Hardcoded for now
            
            foreach ($request->input('items') as $item) {
                $newData = [
                    "order_id" => $cart->id,
                    "item_id" => $item["id"],
                    "item_type" => $itemType,
                    "item_count" => floatval($item["count"])
                ];
                
                array_push($data, $newData);
            }
            $cart->packages()->sync($data);
            $cart->services()->sync($data);

            // Calculate New Total


            $service_ids =  array_map(function($item){
                return $item["id"];
            }, $request->input('items'));

            $services = DB::table("m_service")
                ->select(["id","price", "minimum_order"])
                ->whereIn('id',$service_ids)
                ->get();

            $total = 0;

            foreach ($service_ids as $index => $id) {
                $index = array_search($id, array_column($services->toArray(), 'id'));
                $reqIndex = array_search($id, array_column($request->input('items'), 'id'));

                $req = $request->input('items')[$reqIndex];
                $service = $services[$index];

                $amount = $req["count"] > $service->minimum_order ?$req["count"] :$service->minimum_order;
                $price = $amount * $service->price;

                $total = $total + $price;
            }

            $cart->total_price = $total;
        }

        if($request->input('userId') != null){
            $cart->user_id = $request->input('userId');
        }
        
        $cart->save();
        return Response::ok($cart);
    }

    function delete(Request $request, $cartId)
    {
        try {
            $cart = Cart::where('id', $cartId)->first();

            if ($cart == null) {
                throw new CoreException("Cart with given id not found!", 404);
            }

            $cart->delete();

            return Response::ok($cart);
        } catch (CoreException $th) {
            return Response::fail($th->getMessage(), $th->getDetails(), $th->getStatus());
        } catch (Exception $th) {
            return Response::fail($th->getMessage(), null, 500);
        }
    }
}
