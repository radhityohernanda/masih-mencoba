<?php

namespace App\Http\Controllers\Api\Cart;

use App\Enum\PaymentStatus;
use App\Exceptions\CoreException;
use App\Models\Cart;
use App\Models\MerchantPaymentMethod;
use App\Models\Order;
use App\Utilities\FileStorage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Utilities\Response;
use App\Models\PaymentProof;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;

class ProofController
{
    public function add(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                "images.*" => "required|base64image|base64max:2048",
                "paymentMethod"=>"required"
            ]);


            if ($validator->fails()) {
                return Response::fail('invalid input', $validator->errors(), 400);
            }

            $method = MerchantPaymentMethod::where('id', $request->input('paymentMethod'))->first();


            if($method == null){
                throw new CoreException('Payment method not found!', 404);
            }

            $cart = Cart::where('id', $id)->lockForUpdate()->first();

            if ($cart == null) {
                throw new CoreException("Order with given id not found!", 404);
            }

            $data = [];

            foreach ($request->input('images') as $index => $value) {
                $file = FileStorage::store($value);

                $temp = [
                    "order_id" => $cart->id,
                    "proof_url" => $file,
                    "created_by"=>Auth::id(),
                    "updated_by"=>Auth::id(),
                    "created_at"=>Carbon::now(),
                    "updated_at"=> Carbon::now()
                ];

                array_push($data, $temp);
            }

            $order = $cart->toOrder();
            $order->payment_status = PaymentStatus::DONE;
            $order->save();

            PaymentProof::insert($data);
            DB::commit();
            
            $order = Order::where('id', $order->id)
            ->with('packages', 'packages.services', 'services', 'proofs')
            ->first();
            return Response::ok($order);
        } catch (CoreException $th) {
            DB::rollBack();
            return Response::fail($th->getMessage(), $th->getDetails(), $th->getStatus());
        } catch (Exception $th) {
            DB::rollBack();
            return Response::fail($th->getMessage(), null, 500);
        }
    }
}
