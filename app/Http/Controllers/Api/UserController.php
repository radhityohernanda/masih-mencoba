<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB as DB;
use App\Models\User;
use App\Utilities\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;
use App\Utilities\FileStorage;

class UserController extends Controller
{
    /**
     * user profile data api
     *
     * @return \Illuminate\Http\Response
     */
    public function profileData($id){
        try{
            $user = User::find($id);

            if($user->image !== null){
                $userImage = $user->image;
                $baseUrl = URL::to('/');
                $user->image = $baseUrl . $userImage;
            }

            return Response::ok([
                "userData"=>$user,
            ]);
        } catch(\Throwable $th){
            return Response::fail("Unauthorized", $th, 401);
        }
    }

    /**
     * Update user profile api
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'image' => 'nullable|base64image|base64max:2048',
                'fcm_token' => 'nullable',
            ]);

            if($validator->fails())
            {
                return Response::fail('invalid input', $validator->errors(), 400);
            }

            $user = User::find($id);
            $user->name = $request->input('name');
            $user->fcm_token = $request->input('fcm_token');

            if($user->email != $request->input('email')){
                if(User::where('email', $request->input('email'))->first() !== null){
                    return Response::fail("email already taken");
                }
                $user->email = $request->input('email');
            }

            if($user->phone != $request->input('phone')){
                if(User::where('phone', $request->input('phone'))->first() !== null){
                    return Response::fail("phone already taken");
                }
                $user->phone = $request->input('phone');
            }

            if($request->input('image')){
                if($user->image !== null){
                    FileStorage::delete($user->image);
                }

                $url = FileStorage::store($request->input("image"),"image/icons/user");

                $user->image = $url;
            }

            if($request->has('deletePhoto')){
                FileStorage::delete($user->image);

                $user->image = null;
            }

            if($request->input('oldPassword') && $request->input('newPassword') ){
                if(!password_verify($request->input('oldPassword'), $user->password)){
                    return Response::fail("invalid old password");
                }
                $user->password = Hash::make($request->input('newPassword'));
            }

            $user->save();

            DB::commit();

            return Response::ok($user, "profile updated");
        } catch(\Throwable $th){
            DB::rollBack();
            return Response::fail('update profile failed');
        }
    }

}
