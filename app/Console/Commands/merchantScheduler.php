<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\MerchantScheduleDay as Day;
use App\Models\Merchant;

class merchantScheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merchant:scheduler';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Merchant daily scheduler status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = Carbon::today()->translatedFormat('l');
        $todaySchedule = Day::where('day', $today)->get();
        $timeNow = Carbon::now()->format('H:i');

        // merchant schedules for today
        foreach($todaySchedule as $day){
            // find schedule time for today
            foreach($day->time as $time){
                // if the schedule empty then merchant is close that day
                if(empty($time->is_24hours) && empty($time->open_at) && empty($time->close_at)){
                    // update close status per merchant
                    foreach($time->merchant as $merchant){
                        if($merchant->is_open){
                            $merchant->is_open = false;
                            $merchant->save();
                        }
                    }
                } else{
                    //if 24 hours then open the merchant that hasn't opened
                    if($time->is_24hours){
                        foreach($time->merchant as $merchant){
                            if(!$merchant->is_open){
                                $merchant->is_open = true;
                                $merchant->save();
                            }
                        }
                    }
                    // if merchant has custom time then is_24hours is false
                    else{
                        // open merchant at custom time
                        if($time->open_at == $timeNow){
                            foreach($time->merchant as $merchant){
                            $merchant->is_open = true;
                            $merchant->save();
                            }
                        }

                        // close merchant at custom time
                        if($time->close_at == $timeNow){
                            foreach($time->merchant as $merchant){
                                $merchant->is_open = false;
                                $merchant->save();
                            }
                        }
                    }
                }
            }
        }
    }
}
