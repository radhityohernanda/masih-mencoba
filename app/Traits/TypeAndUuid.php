<?php

namespace App\Traits;

use Illuminate\Support\Str;
use App\Scopes\TypeScope;

trait TypeAndUuid
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = (string) Str::uuid(); // generate uuid
                $model->type = (string) get_class($model);
                // Change id with your primary key
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
        static::addGlobalScope(new TypeScope);
    }
}
