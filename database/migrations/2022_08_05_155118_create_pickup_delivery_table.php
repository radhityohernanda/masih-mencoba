<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePickupDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_pickup_delivery', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->enum('type', ['OFFLINE', 'ONLINE']);
            $table->string('distance');
            $table->string('max_distance');
            $table->bigInteger('shipping_charges');
            $table->boolean('status')->default(true);
            $table->timestampTz('deleted_at')->nullable();
            $table->timestampsTz();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->uuid('merchant_id');
            $table->foreign('merchant_id')->references('id')->on('m_merchant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_pickup_delivery');
    }
}
