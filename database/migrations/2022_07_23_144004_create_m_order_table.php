<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_order', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->decimal('total_price', 10 ,2);
            $table->enum('payment_status', ['0','1', '2', '3', '4'])->comment('0=cart, 1=waiting, 2=done, 3=expired, 4=cancel');
            $table->uuid('user_id');
            $table->uuid('merchant_id')->nullable();
            $table->timestampsTz();

            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();

            $table->timestampTz('deleted_at')->nullable();
            $table->foreign('user_id')->references('id')->on('m_user');
            $table->foreign('merchant_id')->references('id')->on('m_merchant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_order');
    }
}
