<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_service', function (Blueprint $table) {
            $table->string('icon')->nullable()->change();
            $table->string('type')->default('App/Service');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_service', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
