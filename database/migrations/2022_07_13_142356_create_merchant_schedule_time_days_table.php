<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantScheduleTimeDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_schedule_time_day', function (Blueprint $table) {
            $table->id();

            $table->uuid('merchant_schedule_day_id')->constrained('m_merchant_schedule_day')->onDelete('cascade');
            $table->uuid('merchant_schedule_time_id')->constrained('m_merchant_schedule_time')->onDelete('cascade');

            $table->timestampsTz();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_schedule_time_day');
        Schema::dropIfExists('merchant_schedule_time_days');
    }
}
