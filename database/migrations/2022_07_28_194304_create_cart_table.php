<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_order', function (Blueprint $table) {
            $table->string('type')->nullable();
            $table->enum('order_type',["ONLINE","OFFLINE","SUBSCRIPTION"])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('m_order',['type','order_type']);
    }
}
