<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMPaymentMethodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_payment_method', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->enum('type', ['BANK', 'DIGITAL_WALLET', 'OTHER']);
            $table->string('name');
            $table->string('account');
            $table->string('description');
            $table->boolean('status')->default(false);
            $table->timestampTz('deleted_at')->nullable();
            $table->timestampsTz();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->uuid('merchant_id');
            $table->foreign('merchant_id')->references('id')->on('m_merchant');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_payment_method');
    }
}
