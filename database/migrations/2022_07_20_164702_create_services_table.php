<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_service', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 200);
            $table->text('description')->nullable();
            $table->uuid('category_id');
            $table->bigInteger('price');
            $table->integer('estimation');
            $table->enum('time',['DAY','HOUR']);
            $table->enum('unit', ['KG','G','PCS']);
            $table->integer('minimum_order');
            $table->boolean('status')->default(FALSE);
            $table->text('icon');
            $table->uuid('merchant_id');
            $table->timestamps();
            $table->uuid('created_by');
            $table->uuid('updated_by');
            $table->uuid('deleted_by')->nullable();
            $table->timestampTz('deleted_at')->nullable();
            $table->foreign('category_id')->references('id')->on('m_category');
            $table->foreign('merchant_id')->references('id')->on('m_merchant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_service');
    }
}
