<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PaymentProof extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_proof',function(Blueprint $table){
            $table->uuid('order_id');
            $table->string('proof_url');
            $table->uuid('updated_by')->nullable();
            $table->uuid('created_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->uuid('deleted_at')->nullable();
            $table->timestampsTz();
            $table->foreign('order_id')->references('id')->on('m_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_proof');
    }
}
