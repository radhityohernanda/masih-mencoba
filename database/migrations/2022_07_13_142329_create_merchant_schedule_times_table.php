<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantScheduleTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_merchant_schedule_time', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('open_at')->nullable();
            $table->string('close_at')->nullable();
            $table->boolean('is_24hours')->nullable();
            $table->timestampsTz();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_merchant_schedule_time');
        Schema::dropIfExists('_merchant_schedule_times');
    }
}
