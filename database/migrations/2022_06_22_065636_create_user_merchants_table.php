<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_merchant', function (Blueprint $table) {
            $table->id();
            $table->uuid('user_id')->constrained('m_user')->onDelete('cascade');
            $table->uuid('merchant_id')->constrained('m_merchant')->onDelete('cascade');

            $table->timestampsTz();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_merchant');
        Schema::dropIfExists('user_merchants');
    }
}
