<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class UpdateMerchantImageNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_merchant', function (Blueprint $table) {
            $table->string('image')->nullable()->change();
            $table->string('image')->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('m_merchant')){
            DB::raw("
            ALTER TABLE m_merchant
                ALTER image SET NOT NULL USING COALESCE(image, 'user.png'),
                ALTER image SET DEFAULT('user.png')");
        }

        if(Schema::hasTable('merchants')){
            DB::raw("
            ALTER TABLE merchants
                ALTER image SET NOT NULL USING COALESCE(image, 'user.png'),
                ALTER image SET DEFAULT('user.png')");
        }
    }
}
