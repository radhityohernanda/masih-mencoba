<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateIsOpenMerchantsFalse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_merchant', function (Blueprint $table) {
            $table->boolean('is_open')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('m_merchant')){

            Schema::table('m_merchant', function (Blueprint $table) {
                $table->boolean('is_open')->default(1)->change();
            });
        }

        if(Schema::hasTable('merchants')){

            Schema::table('merchants', function (Blueprint $table) {
                $table->boolean('is_open')->default(1)->change();
            });
        }
    }
}
