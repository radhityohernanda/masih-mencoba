<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_merchant', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('phone');
            $table->string('address');
            $table->string('bank_name')->nullable();
            $table->boolean('is_open')->default(false);
            $table->string('time_start')->nullable();
            $table->string('time_end')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->double('longitude')->nullable();
            $table->double('latitude')->nullable();
            $table->uuid('parent_id')->nullable();
            $table->string('image')->default('merchant.png');
            $table->timestampsTz();

            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();

            $table->timestampTz('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // WARNING POSTGRES ONLY

        if(Schema::hasTable('m_merchant')){
            DB::statement('DROP TABLE IF EXISTS m_merchant CASCADE');
        }

        if(Schema::hasTable('merchants')){
            DB::statement('DROP TABLE IF EXISTS merchants CASCADE');
        }

    }
}
