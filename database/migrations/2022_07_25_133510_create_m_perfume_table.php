<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMPerfumeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_perfume', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->text('name')->unique();
                $table->boolean('is_available')->default(false);
                $table->timestampTz('created_at');
                $table->timestampTz('updated_at');
                $table->timestampTz('deleted_at')->nullable();
                $table->uuid('created_by');
                $table->uuid('updated_by');
                $table->uuid('deleted_by')->nullable();
                $table->uuid('merchant_id');
                $table->foreign('merchant_id')->references('id')->on('m_merchant');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_perfume');
    }
}
