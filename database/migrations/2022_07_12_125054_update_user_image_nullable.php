<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateUserImageNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_user', function (Blueprint $table) {
            $table->string('image')->nullable()->change();
            $table->string('image')->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // WARNING POSTGRES ONLY
        if (Schema::hasTable('m_user')) {
            DB::raw("
                ALTER TABLE m_user 
                    ALTER image SET NOT NULL USING COALESCE(image, 'user.png'),
                    ALTER image SET DEFAULT('user.png')
            ");
        }

        if (Schema::hasTable('users')) {
            DB::raw("
            ALTER TABLE users 
                ALTER image SET NOT NULL USING COALESCE(image, 'user.png'),
                ALTER image SET DEFAULT('user.png')
        ");
        }
    }
}
