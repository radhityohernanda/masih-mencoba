<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyToPaymentProof extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_proof', function (Blueprint $table) {
            $table->uuid('payment_method_id');
            $table->foreign('payment_method_id')->references('id')->on('m_payment_method')
                ->onDelete("RESTRICT");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_proof', function (Blueprint $table) {
            //
        });
    }
}
