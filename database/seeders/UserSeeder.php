<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_user')->insert([
            'id' => Str::uuid()->toString(),
            'name' => 'Edo',
            'email' => 'edoaja@email.com',
            'phone' => '+6281212121212',
            'password' => Hash::make('edoaja123'),
            'verified_at' => date('Y-m-d')
        ]);
    }
}
