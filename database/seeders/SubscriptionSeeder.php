<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_subscription')->insert([
            ['id' => Str::uuid()->toString(), 'name' => '1 bulan', 'description' => 'Langganan dengan paket 1 bulan',
            'price' => 100000, 'type_total' => '1' , 'type' => 'month'],
            ['id' => Str::uuid()->toString(), 'name' => '6 bulan', 'description' => 'Langganan dengan paket 6 bulan',
            'price' => 550000, 'type_total' => '6' , 'type' => 'month'],
            ['id' => Str::uuid()->toString(), 'name' => '1 tahun', 'description' => 'Langganan dengan paket 1 bulan',
            'price' => 1000000, 'type_total' => '1' , 'type' => 'year'],
        ]);
    }
}
