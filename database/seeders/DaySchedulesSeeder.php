<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DaySchedulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_merchant_schedule_day')->insert([
            ['id' => Str::uuid()->toString(), 'day' => 'Senin'],
            ['id' => Str::uuid()->toString(), 'day' => 'Selasa'],
            ['id' => Str::uuid()->toString(), 'day' => 'Rabu'],
            ['id' => Str::uuid()->toString(), 'day' => 'Kamis'],
            ['id' => Str::uuid()->toString(), 'day' => 'Jumat'],
            ['id' => Str::uuid()->toString(), 'day' => 'Sabtu'],
            ['id' => Str::uuid()->toString(), 'day' => 'Minggu']
        ]);
    }
}
