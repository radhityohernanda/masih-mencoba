
# Turn on maintenance mode
php artisan down || true

# Run database migrations
php artisan migrate --force

# Insert seeder
php artisan db:seed

# Clear caches
php artisan cache:clear

# Clear expired password reset tokens
php artisan auth:clear-resets

# Clear and cache routes
php artisan route:cache

# Clear and cache config
php artisan config:cache

# Clear and cache views
php artisan view:cache

# Run queue work
php artisan queue:work

# Run storage link
php artisan storage:link

# Turn off maintenance mode
php artisan up
