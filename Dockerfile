#
# Stage 1 - PHP Dependencies
#
FROM composer:2 as vendor

COPY database/ database/
COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install \
    --ignore-platform-reqs \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --prefer-dist

#
# Stage 2 - Frontend Dependencies
#
FROM node:15.10-alpine as frontend

RUN mkdir -p /app/public

COPY package.json webpack.mix.js /app/
COPY resources/ /app/resources/

WORKDIR /app
RUN npm install
RUN npm install laravel-mix@latest
RUN npm run production

#
# Stage 3 - Run Application
#
FROM php:8.0-apache-buster

RUN apt-get update && apt-get install -y libpq-dev libpng-dev libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev libfreetype6-dev
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install pdo pdo_pgsql gd

COPY . /var/www/html
COPY --from=vendor /app/vendor/ /var/www/html/vendor/
COPY --from=frontend /app/public/js/ /var/www/html/public/js/
COPY --from=frontend /app/public/css/ /var/www/html/public/css/
COPY --from=frontend /app/mix-manifest.json /var/www/html/mix-manifest.json

RUN chmod -R 755 /var/www/html && chmod -R 777 /var/www/html/bootstrap/cache
RUN chmod -R o+w /var/www/html/storage
RUN chown -R www-data:www-data /var/www/html

ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN a2enmod rewrite

RUN cp .env.example .env