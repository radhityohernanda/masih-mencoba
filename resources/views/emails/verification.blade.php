<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Selamat Datang di iya laundry</title>
    <style>
        body {
            background-color: #F1F2F4;
        }

        .email {
            font-family: Arial, Helvetica, sans-serif;
            margin-top: 3rem;
        }

        .header {
            background-color: #05A6EA;
            text-align: center;
            color: #fff;
            padding: 1px;
        }

        .body {
            padding: 10px;
            background-color: #FFF;
        }

        .content {
            box-shadow: 6px 10px 15px 1px rgba(0, 0, 0, 0.51);
            max-width: 600px;
            margin: 0 auto;
        }

        .text {
            font-size: 12pt;
            text-align: justify;
        }

        .text-small {
            font-size: 10pt;
            text-align: justify;
        }

        .otp {
            text-align: center;
        }

        .footer {
            background-color: #05A6EA;
            text-align: center;
            color: #fff;
            padding: 10px;
        }
    </style>
</head>

<body>
    <div class="email">
        <div class="content">
            <div class="header">
                <h2>Iya-Laundry</h2>
            </div>
            <div class="body">
                <h3>Halo {{ $user->name }}, Selamat Datang di Iya-Laundry!</h3>
                <p class="text">
                    Selamat datang di Iya-Laundry! Berikut adalah kode OTP anda, silahkan masukan
                    kode OTP berikut ke dalam aplikasi Iya-Laundry. Ingat untuk tidak memberikan kode ini
                    kepada siapa pun termasuk kepada kami!
                </p>
                <div class="otp">
                    <h1>{{ $otp->otp }}</h1>
                </div>
                <p class="text">
                    Terima kasih telah menggunakan layanan Iya-Laundry!
                </p>
            </div>
            <div class="footer">
                <p class="text">Iya-Laundry</p>
                <p class="text-small">Jl. Kebangsaan Timur Nomer 5</p>
            </div>
        </div>
    </div>
</body>

</html>
