require('../sass/app.scss')
import Vue from 'vue';
import App from './App.vue';

import VueRouter from 'vue-router';
import router from './routes/router';
Vue.use(VueRouter);

import test from './components/ExampleComponent.vue'

// import VueAxios from 'vue-axios';
// import axios from 'axios';
// Vue.use(VueAxios, axios);

new Vue(Vue.util.extend({ router }, App)).$mount('#app');

Vue.component('example-component', require(test));
Vue.component('test' )

const app = new Vue({
    el: '#app',
});