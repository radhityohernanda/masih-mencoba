# FRONT END SQUAD
This directory is for frontend management under the route of `/admin-area`

## Get Started
--- 
To start development run the following command 

1. Install composer dependencies `composer install`
2. Install node.js dependencies `npm install`
3. Run laravel mix `npm run dev` or `npm run watch`
4. Serve the app `php artisan serve`

## Rules
---
### Routes
- All routes should be managed by `vue-router`
- All routes configuration are stored under `routes` folder
- Create a file under `/routes` folder corresponding to the main route. For example if you will have `/admin-area/dashboard` and `/admin-area/report` route create a file called `dashboard.js` and `report.js` then register both in `router.js` inside the children properties of `/admin-area` path.
- If you need to add route other than `/admin-area` pelase add it before `'*'` path

### Layouts
- All layouts should be managed under `/layouts` folder

### Views
- All pages / views are managed under `/views/` folder
- If you need to create views / pages create it under `/views/{your-feature}/` folder. For example if you want to create views for bot `dashboard` and `report` create it in each individual folder called `/views/dashboard/{your-feature}.vue` and `/views/report/{your-feature}.vue`

### Components
- Components SHOULD be reusable
- Create components under `/components` folder
- Organise your components and categorized it wisely
- Create a folder for each category. For example if you have `DragNDrop` component for a form place it under `/components/form` folder

### State Management
- State should be managed by vuex
- Minimize API call and Maximize state management usage
- All state configuration stored under `store` folder
- Organize state according to it's configuration. For example if you need an auth state, please create a file called `/store/auth.js` and manage it there.

### Dependencies
- Add dependencies if you think it is required
- DO NOT add dependencies that is not reusable

### Style
- Create your style in sass language
- Add your style in `{dirname}/resources/sass/app.scss`
- Use variable for reusable value such as color anywhere you can

### Other
- You should never need to change any laravel files. If you think you need one, ask your higher up.
- DO NOT CREATE AN API BY YOUR SELF, ask backend guy to do so.
