import Vue from 'vue'
import VueRouter from 'vue-router'
import dashboard from './dashboard'

Vue.use(VueRouter);
export default new VueRouter({
    mode: 'history',
    scrollBehavior: (to, from, savedPosition) => ({ y: 0 }), 
    routes: [
        ...dashboard
    ],
})