import VueRouter from 'vue-router'
import dashboard from './dashboard';
export default new VueRouter({
    mode: 'history',
    scrollBehavior: (to, from, savedPosition) => ({ y: 0 }), 
    routes: [
        {
            path: '/admin-area',
            component:()=>import('../views/adminArea'),
            children:[
                ...dashboard
            ]
        },
        {
            path:'*',
            name: '404',
            component: ()=>import('../views/errors/404')
        }
    ],
});