<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'auth'], function () {

    //API route for register new user
    Route::post('/register', 'Api\Auth\RegisterController@register')->name('api.register');

    // API route for resending otp
    Route::post('/resend/otp', 'Api\Auth\RegisterController@resendOtp')->name('api.resend.otp');

    //API route for OTP verify
    Route::post('/verify', 'Api\Auth\OtpVerificationController@verify')->name('api.verify');

    //API route for SSO
    Route::post('/sso', 'Api\Auth\SsoController@login')->name('api.sso');

    //API route for login user
    Route::post('/login', 'Api\Auth\LoginController@login')->name('api.login');

    //API route for failed authentication
    Route::get('/failed', 'Auth\FailedAuthenticationController@fail')->name('auth.fail');

    // API route for logout user
    Route::post('/logout', 'Api\Auth\LoginController@logout')->name('api.logout')->middleware('auth:sanctum');

    // Forgot Password
    Route::post('/password/forgot', 'Api\Auth\PasswordController@requestOtp')->name('api.password.forgot');

    // Reset Password
    Route::post('/password/reset', 'Api\Auth\OtpVerificationController@reset')->name('api.password.reset');
});

//API route for checking account if exist
Route::post('/user/check', 'Api\Auth\UserCheckController@userCheck')->name('api.userCheck');
Route::post('/user/check/email', 'Api\Auth\UserCheckController@userCheckByEmail')->name('api.userCheckByEmail');

//APi route for user profile
Route::get('/user/{id}', 'Api\UserController@profileData');
Route::put('/user/{id}', 'Api\UserController@update');

// Route for payment notification
Route::post('/payment/notification', 'Api\Merchant\SubscriptionController@receive');

Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {

    //APi route for merchant profile
    Route::group(['prefix' => 'merchant'], function () {
        Route::get('/subscription', 'Api\Merchant\SubscriptionController@get');

        //API route for merchant pickup and delivery data
        Route::group(['prefix' => 'pickup'], function () {
            Route::get('', 'Api\Merchant\PickupDeliveryController@list');
            Route::get('/{id}', 'Api\Merchant\PickupDeliveryController@get');
            Route::post('', 'Api\Merchant\PickupDeliveryController@create');
            Route::put('/{id}', 'Api\Merchant\PickupDeliveryController@update');
            Route::delete('/{id}', 'Api\Merchant\PickupDeliveryController@delete');
        });
         //API route for merchant perfume
         Route::group(['prefix'=>'perfume'], function(){
            Route::post('', 'Api\Merchant\PerfumeController@create');
            Route::put('/{id}', 'Api\Merchant\PerfumeController@update');
            Route::delete('/{id}', 'Api\Merchant\PerfumeController@delete');
            Route::get('/{id}', 'Api\Merchant\PerfumeController@get');
            Route::get('', 'Api\Merchant\PerfumeController@list');
        });

        //API route for merchant payment 
        Route::group(['prefix' => 'payment'], function () {
            Route::get('', 'Api\Merchant\PaymentMethodController@list');
            Route::get('/{id}', 'Api\Merchant\PaymentMethodController@get');
            Route::post('', 'Api\Merchant\PaymentMethodController@create');
            Route::put('/{id}', 'Api\Merchant\PaymentMethodController@update');
            Route::delete('/{id}', 'Api\Merchant\PaymentMethodController@delete');
        });

        // ROUTE FOR MERCHANT'S CART
        Route::group(['prefix' => 'cart'], function () {
            Route::get('', 'Api\Cart\CartController@list');
            Route::post('', 'Api\Cart\CartController@create');
            Route::get('/{id}', 'Api\Cart\CartController@read');
            Route::post('/{id}/proof', 'Api\Cart\ProofController@add');
            Route::put('/{id}', 'Api\Cart\CartController@update');
            Route::delete('/{id}', 'Api\Cart\CartController@delete');
        });

         // ROUTE FOR MERCHANT'S CART
         Route::group(['prefix' => 'service-package'], function () {
            Route::get('', 'Api\Merchant\ServicePackageController@list');
            Route::post('', 'Api\Merchant\ServicePackageController@create');
            Route::get('/{id}', 'Api\Merchant\ServicePackageController@read');
            // Route::put('/{id}', 'Api\Merchant\ServicePackageController@update');
            Route::delete('/{id}', 'Api\Merchant\ServicePackageController@delete');
        });

        Route::get('', 'Api\Merchant\MerchantController@getAll');
        Route::get('/parent', 'Api\Merchant\MerchantController@getAllParent');
        Route::get('/{id}', 'Api\Merchant\MerchantController@get');
        Route::post('/{parentId?}', 'Api\Merchant\MerchantController@create');
        Route::put('/{id}', 'Api\Merchant\MerchantController@update');
        Route::delete('/{id}', 'Api\Merchant\MerchantController@delete');
        Route::put('/{id}/status', 'Api\Merchant\MerchantController@changeStatus');
        Route::put('/{id}/schedule', 'Api\Merchant\MerchantController@updateSchedule');

        Route::post('/{id}/subscription/order', 'Api\Merchant\SubscriptionController@order');
    });


    Route::get('/profile', function (Request $request) {
        return Auth()->user();
    });


    // ROUTE FOR MERCHANT'S SERVICE CATEGORY
    Route::group(['prefix' => 'category'], function () {
        Route::get('', 'Api\Category\CategoryController@list');
        Route::post('', 'Api\Category\CategoryController@create');
        Route::get('/{id}', 'Api\Category\CategoryController@read');
        Route::put('/{id}', 'Api\Category\CategoryController@update');
        Route::delete('/{id}', 'Api\Category\CategoryController@delete');
    });

    // ROUTE FOR MERCHANT'S SERVICE
    Route::group(['prefix' => 'service'], function () {
        Route::get('', 'Api\Service\ServiceController@list');
        Route::post('', 'Api\Service\ServiceController@create');
        Route::get('/{id}', 'Api\Service\ServiceController@read');
        Route::put('/{id}', 'Api\Service\ServiceController@update');
        Route::delete('/{id}', 'Api\Service\ServiceController@delete');
    });

    // ROUTE FOR MERCHANT'S PACKAGE
    Route::group(['prefix' => 'package'], function () {
        Route::get('', 'Api\Service\PackageController@list');
        Route::post('', 'Api\Service\PackageController@create');
        Route::get('/{id}', 'Api\Service\PackageController@read');
        Route::put('/{id}', 'Api\Service\PackageController@update');
        Route::delete('/{id}', 'Api\Service\PackageController@delete');
    });

    // ROUTE FOR CUSTOMER HELP CENTER       
    Route::group(['prefix'=>'helpcenter'], function(){
        Route::post('', 'Api\HelpCenterController@create');  
        Route::put('/{id}', 'Api\HelpCenterController@update');
        Route::delete('/{id}', 'Api\HelpCenterController@delete');
        Route::get('', 'Api\HelpCenterController@getAll');
    });    

    //API route for costumer saved address
    Route::group(['prefix' => 'address'], function () {
        Route::get('', 'Api\AddressController@list');
        Route::get('/{id}', 'Api\AddressController@get');
        Route::post('', 'Api\AddressController@create');
        Route::put('/{id}', 'Api\AddressController@update');
        Route::delete('/{id}', 'Api\AddressController@delete');
    });

});
    
    
    
