<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('testing');
});


Auth::routes(['verify' => true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(["prefix"=>'admin-area'], function(){
    Route::get('/{any?}', [
        function () {
            return view('admin');
        }
    ])->where('any', '.*')->name('admin-area');
});


Route::get('/dashboard', 'Backend\DashboardController@index')->name('dashboard');


